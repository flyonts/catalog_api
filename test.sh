#!/bin/bash
tempdir=../test_data/$RANDOM
mkdir -p $tempdir/schema
cp prozorro/catalog/schema/*.json $tempdir/schema/ 
export CATALOG_SECRET=please-change
export CATALOG_DATA=$tempdir
export CATALOG_CREATE=YES
export CATALOG_CACHE=YES
export CATALOG_CHANGES=YES
export CATALOG_METHOD_OVERRIDE=YES
export CATALOG_PROXY_FIX=YES
export TEST_RUN_SERVER=YES
export TEST_BASIC_AUTH=test:test
# update .coveragerc if test with CATALOG_CHANGES
# echo -e "[run]\nconcurrency = gevent" > .coveragerc
pytest -x --cov=prozorro.catalog prozorro/catalog/tests
rm -rf $tempdir
