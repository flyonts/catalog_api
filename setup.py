#!/usr/bin/env python

from setuptools import setup, find_packages

requires = [
    'flask',
    'iso8601',
    'jsonschema',
    'python-dateutil',
    'python-rapidjson',
]

console_scripts = [
    'create_catalog_database = prozorro.catalog:create_catalog_database',
    'verify_catalog_database = prozorro.catalog:verify_catalog_database',
    'run_as_websocket_server = prozorro.catalog:run_as_websocket_server'
]

setup(
    name='prozorro.catalog',
    version='0.2.2',
    description='Prozorro Catalog Central Database API',
    author='Volodymyr Flonts',
    author_email='flyonts@gmail.com',
    license='Apache License 2.0',
    url='https://bitbucket.org/flyonts/catalog_api',
    packages=find_packages(),
    namespace_packages=['prozorro'],
    package_data={'prozorro.catalog': 'schema/*.json'},
    entry_points={'console_scripts': console_scripts},
    include_package_data=True,
    install_requires=requires,
    zip_safe=False
)
