#!/bin/sh
export FLASK_APP=prozorro.catalog
export FLASK_ENV=development
export CATALOG_SECRET=please-change
export CATALOG_DATA=../catalog_data
export CATALOG_CACHE=YES
export CATALOG_CHANGES=YES
export CATALOG_JSON_AS_ASCII=
#exec flask run
run_as_websocket_server
