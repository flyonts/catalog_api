from . import api
from random import randint


def test_600_image_create():
    files = {'image': [
        'test.png',
        b'\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\x01\x00' +
        b'\x00\x00\x01\x01\x03\x00\x00\x00%\xdbV\xca\x00\x00\x00' +
        b'\x03PLTE\x00\x00\x00\xa7z=\xda\x00\x00\x00\x01tRNS\x00' +
        b'@\xe6\xd8f\x00\x00\x00\nIDAT\x08\xd7c`\x00\x00\x00\x02' +
        b'\x00\x01\xe2!\xbc3\x00\x00\x00\x00IEND\xaeB`\x82',
        'image/png'
    ]}
    data = {'sizes': '10x10', 'title': 'Test image title'}
    resp = api.post('images', data=data)
    assert resp.status_code == 400
    assert 'No image' in resp.text

    files['image'][0] = 'test.dat'
    resp = api.post('images', data=data, files=files)
    assert resp.status_code == 400
    assert 'Bad filename' in resp.text

    files['image'][0] = 'test%s.png' % randint(100000, 900000)
    resp = api.post('images', data=data, files=files)
    assert resp.status_code == 201
    resp_json = resp.json()
    assert resp_json['data']['sizes'] == data['sizes']
    assert resp_json['data']['title'] == data['title']

    resp = api.post('images', data=data, files=files)
    assert resp.status_code == 400
    assert 'Image already exists' in resp.text


def test_610_image_list():
    resp = api.get('images')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0

    image_id = resp_json['data'][0]['id']
    resp = api.get('images/%s' % image_id)
    assert resp.status_code == 200

    image_id += '-non-exists'
    resp = api.get('images/%s' % image_id)
    assert resp.status_code == 404
