from . import api
from os import environ
from copy import deepcopy
from random import randint
from urllib.parse import quote

test_category = {
  "data": {
    "additionalClassifications": [
      {
        "description": "Засоби індивідуального захисту (респіратори та маски) без клапану",
        "id": "5011020",
        "scheme": "KMU777"
      }
    ],
    "classification": {
      "description": "Медичне обладнання та вироби медичного призначення різні",
      "id": "33190000-8",
      "scheme": "ДК021"
    },
    "description": "Засоби індивідуального захисту (респіратори та маски) без клапану",
    "images": [
      {
        "sizes": "200x200",
        "url": "https://catalog.openprocurement.org.ua/images/mask.200x200.jpg"
      }
    ],
    "procuringEntity": {
      "address": {
        "countryName": "Україна",
        "locality": "Київ",
        "postalCode": "02200",
        "region": "Київ",
        "streetAddress": "вулиця Ушинського, 40"
      },
      "contactPoint": {
        "email": "czo.moza@gmail.com",
        "faxNumber": "0445550055",
        "name": "Надія Бігун",
        "telephone": "380509995906",
        "url": "http://czo.moz.gov.ua/"
      },
      "identifier": {
        "id": "10033300",
        "legalName": "Тестова центральна закупівельна організація \"МЕДИЧНА\"",
        "scheme": "UA-EDR"
      },
      "name": "Тестова центральна закупівельна організація \"МЕДИЧНА\"",
      "kind": "central"
    },
    "title": "Респіратори та маски без клапану",
    "status": "active"
  }
}

patch_category_bad = {
  "data": {
    "procuringEntity": {
      "address": {
        "countryName": "Україна",
        "locality": "Київ",
        "postalCode": "02200",
        "region": "Київ",
        "streetAddress": "вулиця Ушинського, 40"
      }
    }
  }
}

patch_category = {
  "data": {
    "title": "Респіратори та маски без клапану (приховані)",
    "status": "hidden"
  }
}


def test_110_category_create():
    resp = api.post('categories', json=test_category)
    assert resp.status_code == 405

    resp = api.put('categories/123')
    assert resp.status_code == 400

    resp = api.post('categories', json=test_category,
        headers={'X-HTTP-METHOD-OVERRIDE': 'GET'})
    assert resp.status_code == 405

    resp = api.put('categories', json=test_category)
    assert resp.status_code == 405

    data = test_category['data']
    category_id = '{}-{}-{}'.format(data['classification']['id'][:8],
        randint(1000, 9999), data['procuringEntity']['identifier']['id'])
    test_category['data']['id'] = category_id

    resp = api.put('categories/%s' % category_id + '-1', json=test_category)
    assert resp.status_code == 400
    assert 'id mismatch' in resp.text

    test_category['data']['id'] = category_id[:18]

    resp = api.put('categories/%s' % category_id[:18], json=test_category)
    assert resp.status_code == 400
    assert 'too small' in resp.text

    test_category['data']['id'] = category_id

    resp = api.put('categories/%s' % category_id, json=test_category,
        auth=('bad_user', 'bad_password'))
    assert resp.status_code == 403
    assert 'Not permitted' in resp.text

    if 'CATALOG_METHOD_OVERRIDE' in environ:
        resp = api.post('categories/%s' % category_id, json=test_category,
            headers={'X-HTTP-METHOD-OVERRIDE': 'PUT'})
    else:   # pragma: no cover
        resp = api.put('categories/%s' % category_id, json=test_category)

    assert resp.status_code == 201
    resp_json = resp.json()
    assert 'access' in resp_json
    assert 'token' in resp_json['access']

    test_category['access'] = dict(resp_json['access'])
    test_dateModified = resp_json['data']['dateModified']

    resp = api.head('categories')
    assert resp.status_code == 200

    resp = api.get('categories')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert set(resp_json['data'][0].keys()) == set(['id', 'dateModified'])
    assert category_id in [i['id'] for i in resp_json['data']]
    items = [i for i in resp_json['data'] if i['id'] == category_id]
    assert len(items) == 1
    assert items[0]['dateModified'] == test_dateModified
    test_category['data']['dateModified'] = test_dateModified

    resp = api.get('categories/..')
    assert resp.status_code in (400, 404)   # depens on wsgi server

    resp = api.get('categories/../../../../../../../../../../etc/passwd')
    assert resp.status_code in (400, 404)

    resp = api.get('categories/%s-bad' % category_id)
    assert resp.status_code == 404

    resp = api.get('categories/%s' % category_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['id'] == category_id

    api.save('test_category', test_category)


def test_111_limit_offset():
    test_category = api.load('test_category')
    test_category_map = dict()
    category_id = test_category['data']['id']
    test_category_map[category_id] = test_category['data']['dateModified']

    for i in range(10):
        test_category_copy = deepcopy(test_category)
        test_category_copy['data']['title'] += " copy {}".format(i + 1)

        data = test_category_copy['data']
        category_id = '{}-{}-{}'.format(data['classification']['id'][:8],
            randint(1000, 9999), data['procuringEntity']['identifier']['id'])
        test_category_copy['data']['id'] = category_id

        resp = api.put('categories/%s' % category_id, json=test_category_copy)
        assert resp.status_code == 201
        resp_json = resp.json()
        assert 'access' in resp_json
        assert 'token' in resp_json['access']

        test_category_map[category_id] = resp_json['data']['dateModified']

    # check for empty limit
    resp = api.get('categories?limit=')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0

    # check for negative limit
    resp = api.get('categories?limit=-1')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0

    # first page forward
    resp = api.get('categories?limit=5')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 5
    prev = resp_json['data'][0]
    assert test_category_map[prev['id']] == prev['dateModified']
    for item in resp_json['data'][1:]:
        assert prev['dateModified'] < item['dateModified']
        assert test_category_map[item['id']] == item['dateModified']
    assert 'next_page' in resp_json
    assert 'offset' in resp_json['next_page']
    offset_normal = resp_json['next_page']['offset']

    # first page backward
    resp = api.get('categories?limit=8&reverse=1')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 8
    prev = resp_json['data'][0]
    assert test_category_map[prev['id']] == prev['dateModified']
    for item in resp_json['data'][1:]:
        assert prev['dateModified'] > item['dateModified']
        assert test_category_map[item['id']] == item['dateModified']
    assert 'next_page' in resp_json
    assert 'offset' in resp_json['next_page']
    offset_reverse = resp_json['next_page']['offset']

    assert offset_normal != offset_reverse

    # second page fowrard
    resp = api.get('categories?limit=10&offset=' + quote(offset_normal))
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 6  # 11 total, first page 5, second 6
    prev = resp_json['data'][0]
    assert test_category_map[prev['id']] == prev['dateModified']
    for item in resp_json['data'][1:]:
        assert prev['dateModified'] < item['dateModified']
        assert test_category_map[item['id']] == item['dateModified']
    assert 'next_page' in resp_json
    assert 'offset' in resp_json['next_page']
    offset_normal = resp_json['next_page']['offset']

    # third page forward (must be empty)
    resp = api.get('categories?limit=10&offset=' + quote(offset_normal))
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 0
    assert 'next_page' not in resp_json

    # second page backward
    resp = api.get('categories?limit=8&reverse=1&offset=' + quote(offset_reverse))
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 3  # 11 total, 8 on first, 3 on second
    prev = resp_json['data'][0]
    assert test_category_map[prev['id']] == prev['dateModified']
    for item in resp_json['data'][1:]:
        assert prev['dateModified'] > item['dateModified']
        assert test_category_map[item['id']] == item['dateModified']
    assert 'next_page' in resp_json
    assert 'offset' in resp_json['next_page']
    offset_reverse = resp_json['next_page']['offset']

    # third page backward (must be empty)
    resp = api.get('categories?limit=8&reverse=1&offset=' + quote(offset_reverse))
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 0


def test_115_category_cache():
    test_category = api.load('test_category')
    category_id = test_category['data']['id']
    dateModified = test_category['data']['dateModified']

    for _ in range(100):
        resp = api.get('categories')
        assert resp.status_code == 200
        assert category_id in resp.text
        assert dateModified in resp.text

    for _ in range(100):
        resp = api.get('categories/%s' % category_id)
        assert resp.status_code == 200
        assert category_id in resp.text
        assert dateModified in resp.text


def test_120_category_patch():
    test_category = api.load('test_category')
    category_id = test_category['data']['id']

    resp = api.patch('categories/%s' % category_id, json=patch_category_bad)
    assert resp.status_code == 401

    patch_category_bad['access'] = {'token': 'bad token value, but long enough'}

    resp = api.patch('categories/%s' % category_id, json=patch_category_bad)
    assert resp.status_code == 403

    patch_category_bad['access'] = dict(test_category['access'])

    resp = api.patch('categories/%s' % category_id, json=patch_category_bad,
        auth=('bad_user', 'bad_password'))
    assert resp.status_code == 403
    assert 'Owner mismatch' in resp.text

    resp = api.patch('categories/%s' % category_id, json=patch_category_bad)
    assert resp.status_code == 400

    resp = api.patch('categories/%s' % category_id, json=patch_category,
        headers={'X-Access-Token': test_category['access']['token']})
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_category['data'].items():
        assert resp_json['data'][key] == patch_value

    test_dateModified = resp_json['data']['dateModified']

    resp = api.get('categories')
    assert resp.status_code == 200
    resp_json = resp.json()
    items = [i for i in resp_json['data'] if i['id'] == category_id]
    assert len(items) == 1
    assert items[0]['dateModified'] == test_dateModified

    patch_category['access'] = dict(test_category['access'])

    resp = api.get('categories/%s' % category_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_category['data'].items():
        assert resp_json['data'][key] == patch_value

    patch_category["data"]["status"] = "active"
    resp = api.patch('categories/%s' % category_id, json=patch_category)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['status'] == 'active'
