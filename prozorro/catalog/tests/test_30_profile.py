from . import api
from copy import deepcopy
from urllib.parse import quote
from random import randint


test_profile = {
  "data": {
    "additionalClassifications": [
      {
        "description": "Засоби індивідуального захисту (респіратори та маски) без клапану",
        "id": "5011020",
        "scheme": "KMU777"
      }
    ],
    "classification": {
      "description": "Медичне обладнання та вироби медичного призначення різні",
      "id": "33190000-8",
      "scheme": "ДК021"
    },
    "criteria": [
      {
        "code": "OCDS-MEDIC-USE-TYPE",
        "description": "Спосіб використання (одноразова або багаторазова)",
        "id": "0001",
        "requirementGroups": [
          {
            "description": "Спосіб використання - одноразова",
            "id": "0001-001",
            "requirements": [
              {
                "dataType": "string",
                "expectedValue": "одноразова",
                "id": "0001-001-01",
                "title": "Одноразова"
              }
            ]
          }
        ],
        "title": "Спосіб використання"
      },
      {
        "code": "OCDS-MEDIC-LAYERS",
        "description": "Кількість шарів тканини, не менше",
        "id": "0002",
        "requirementGroups": [
          {
            "description": "Кількість шарів тканини, не менше 3-х",
            "id": "0002-001",
            "requirements": [
              {
                "dataType": "integer",
                "id": "0002-001-01",
                "minValue": 3,
                "pattern": "[0-9]*",
                "title": "Три шари або більше",
                "unit": {
                  "code": "S10",
                  "name": "шарів тканини"
                }
              }
            ]
          }
        ],
        "title": "Кількість шарів"
      },
      {
        "code": "OCDS-MEDIC-PACKING-COUNT",
        "description": "Одиниця пакування (не менше, штук)",
        "id": "0003",
        "requirementGroups": [
          {
            "description": "Не менше 50 шт/упаковка",
            "id": "0003-001",
            "requirements": [
              {
                "dataType": "integer",
                "id": "0003-001-01",
                "minValue": 50,
                "pattern": "[0-9]*",
                "title": "50 штук",
                "unit": {
                  "code": "H73",
                  "name": "штука"
                }
              }
            ]
          }
        ],
        "title": "Одиниця пакування"
      },
      {
        "code": "OCDS-MEDIC-NOSE-CLIP",
        "description": "Виріб оснащений носовим зажимом (носовою кліпсою)",
        "id": "0004",
        "requirementGroups": [
          {
            "description": "Виріб оснащений носовим зажимом (носовою кліпсою)",
            "id": "0004-001",
            "requirements": [
              {
                "dataType": "boolean",
                "description": "Виріб має бути оснащений носовим зажимом",
                "expectedValue": True,
                "id": "0004-001-01",
                "title": "Виріб оснащений носовим зажимом"
              }
            ]
          }
        ],
        "title": "Носовий зажим (кліпса)"
      },
      {
        "code": "OCDS-MEDIC-ALL-ANY-ONE",
        "description": "Тест на відповідність allOf, anyOf, oneOf",
        "id": "0005",
        "requirementGroups": [
          {
            "description": "Тест всім вимогам",
            "id": "0005-001",
            "requirements": [
              {
                "dataType": "string",
                "id": "0005-001-01",
                "title": "Тест allOf",
                "allOf": ["ALL_A", "ALL_B", "ALL_C"]
              }
            ]
          },
          {
            "description": "Тест всім вимогам",
            "id": "0005-002",
            "requirements": [
              {
                "dataType": "string",
                "id": "0005-002-01",
                "title": "Тест allOf",
                "anyOf": ["ANY_A", "ANY_B", "ANY_C"]
              }
            ]
          },
          {
            "description": "Тест всім вимогам",
            "id": "0005-003",
            "requirements": [
              {
                "dataType": "string",
                "id": "0005-003-01",
                "title": "Тест allOf",
                "oneOf": ["ONE_A", "ONE_B", "ONE_C"]
              }
            ]
          }
        ],
        "title": "Тест allOf, anyOf, oneOf"
      }
    ],
    "description": "Маска медична захисна, гіпоалергенна, одноразова, з доставкою",
    "images": [
      {
        "sizes": "200x200",
        "url": "https://catalog.openprocurement.org.ua/images/mask.200x200.jpg"
      }
    ],
    "status": "active",
    "title": "Маска медична захисна, гіпоалергенна, одноразова, з доставкою",
    "unit": {
      "code": "H70",
      "name": "упаковка"
    },
    "value": {
      "amount": 50,
      "currency": "UAH",
      "valueAddedTaxIncluded": True
    }
  }
}

patch_profile_bad = {
  "data": {
    "id": "new-id"
  }
}

patch_profile = {
  "data": {
    "title": "Маска (прихована)",
    "status": "hidden"
  }
}


def test_310_profile_create():
    test_category = api.load('test_category')
    category_id = test_category['data']['id']
    profile_id = '{}-{}'.format(randint(100000, 900000), category_id)

    test_profile['data']['id'] = profile_id
    test_profile['data']['relatedCategory'] = category_id
    test_profile['access'] = dict(test_category['access'])

    resp = api.get('profiles/%s' % profile_id)
    assert resp.status_code == 404

    resp = api.patch('profiles/%s' % profile_id, json=test_profile)
    assert resp.status_code == 404

    resp = api.put('profiles/%s' % profile_id + '-1', json=test_profile)
    assert resp.status_code == 400
    assert 'id mismatch' in resp.text

    resp = api.put('profiles/%s' % profile_id, json=test_profile)
    assert resp.status_code == 201
    resp_json = resp.json()
    assert resp_json['data']['id'] == test_profile['data']['id']
    assert 'access' in resp_json
    assert 'token' in resp_json['access']
    test_dateModified = resp_json['data']['dateModified']

    test_profile_copy = test_profile.copy()
    test_profile['access'] = resp_json['access']

    resp = api.put('profiles/%s' % profile_id, json=test_profile_copy)
    assert resp.status_code == 400

    resp = api.get('profiles')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0
    assert set(resp_json['data'][0].keys()) == set(['id', 'dateModified'])
    assert profile_id in [i['id'] for i in resp_json['data']]

    resp = api.get('profiles/%s' % profile_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['id'] == test_profile['data']['id']
    assert resp_json['data']['dateModified'] == test_dateModified
    test_profile['data']['dateModified'] = test_dateModified

    api.save('test_profile', test_profile)


def test_311_profile_limit_offset():
    test_category = api.load('test_category')
    category_id = test_category['data']['id']
    test_profile = api.load('test_profile')
    test_profile_map = dict()
    profile_id = test_profile['data']['id']
    test_profile_map[profile_id] = test_profile['data']['dateModified']

    # create 10 profiels (11 in total)
    for i in range(10):
        test_profile_copy = deepcopy(test_profile)
        test_profile_copy['data']['title'] += " copy {}".format(i + 1)
        profile_id = '{}-{}'.format(randint(100000, 900000), category_id)
        test_profile_copy['data']['id'] = profile_id
        test_profile_copy['data']['relatedCategory'] = category_id
        test_profile_copy['access'] = dict(test_category['access'])

        resp = api.put('profiles/%s' % profile_id, json=test_profile_copy)
        assert resp.status_code == 201
        resp_json = resp.json()
        assert resp_json['data']['id'] == test_profile_copy['data']['id']
        assert 'access' in resp_json
        assert 'token' in resp_json['access']

        test_profile_map[profile_id] = resp_json['data']['dateModified']

    offset = ''
    for i in range(4):
        resp = api.get('profiles?limit=5&offset=' + quote(offset))
        assert resp.status_code == 200
        resp_json = resp.json()
        if i == 3:
            assert len(resp_json['data']) == 0
            assert 'next_page' not in resp_json
            break
        assert len(resp_json['data']) > 0
        assert len(resp_json['data']) <= 5
        assert 'next_page' in resp_json
        assert 'offset' in resp_json['next_page']
        offset = resp_json['next_page']['offset']
        prev = resp_json['data'][0]
        assert test_profile_map[prev['id']] == prev['dateModified']
        for item in resp_json['data'][1:]:
            assert prev['dateModified'] < item['dateModified']
            assert test_profile_map[item['id']] == item['dateModified']

    resp = api.get('profiles?reverse=1')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0
    assert 'next_page' in resp_json
    assert 'offset' in resp_json['next_page']
    prev = resp_json['data'][0]
    assert test_profile_map.pop(prev['id']) == prev['dateModified']
    for item in resp_json['data'][1:]:
        assert prev['dateModified'] > item['dateModified']
        assert test_profile_map.pop(item['id']) == item['dateModified']

    assert len(test_profile_map) == 0


def test_320_profile_patch():
    test_profile = api.load('test_profile')
    profile_id = test_profile['data']['id']

    resp = api.get('profiles/%s' % profile_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert profile_id == resp_json['data']['id']

    resp = api.patch('profiles/%s' % profile_id, json=patch_profile_bad)
    assert resp.status_code == 401

    patch_profile_bad['access'] = {'token': 'bad access token, but long enough'}

    resp = api.patch('profiles/%s' % profile_id, json=patch_profile_bad)
    assert resp.status_code == 403

    patch_profile_bad['access'] = dict(test_profile['access'])

    resp = api.patch('profiles/%s' % profile_id, json=patch_profile_bad)
    assert resp.status_code == 400

    patch_profile['access'] = dict(test_profile['access'])

    resp = api.patch('profiles/%s' % profile_id, json=patch_profile)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_profile['data'].items():
        assert resp_json['data'][key] == patch_value

    resp = api.get('profiles/%s' % profile_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_profile['data'].items():
        assert resp_json['data'][key] == patch_value

    patch_profile["data"]["status"] = "active"
    resp = api.patch('profiles/%s' % profile_id, json=patch_profile)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['status'] == 'active'

