from . import api
from os import environ
from rapidjson import dumps, loads


def test_710_changes_feed():
    if 'CATALOG_CHANGES' not in environ:    # pragma: no cover
        return
    from gevent import sleep
    from websocket import create_connection, WebSocketTimeoutException
    changes_url = api.base_url.replace('http://', 'ws://') + 'changes'
    ws = create_connection(changes_url, timeout=1)
    # clear queue from old messages
    try:
        for i in range(100):
            ws.recv()
    except WebSocketTimeoutException:
        pass
    ws.send(dumps({"ping": "hello"}))
    sleep(0.1)
    ws_resp = loads(ws.recv())
    assert ws_resp['pong'] == "hello"

    test_category = api.load('test_category')
    category_id = test_category['data']['id']
    category_patch = {"data": {"title": "Тест змін категорії"}}
    category_patch['access'] = test_category['access']

    resp = api.patch('categories/%s' % category_id, json=category_patch)
    assert resp.status_code == 200
    patch_resp = resp.json()
    sleep(0.1)
    ws_resp = loads(ws.recv())
    assert ws_resp['model'] == 'category'
    assert ws_resp['data']['id'] == category_id
    assert ws_resp['data']['dateModified'] == patch_resp['data']['dateModified']

    ws2 = create_connection(changes_url, timeout=1)

    test_profile = api.load('test_profile')
    profile_id = test_profile['data']['id']
    profile_patch = {"data": {"title": "Тест змін профілю"}}
    profile_patch['access'] = dict(test_profile['access'])

    resp = api.patch('profiles/%s' % profile_id, json=profile_patch)
    assert resp.status_code == 200
    patch_resp = resp.json()
    sleep(0.1)
    ws2_resp = loads(ws2.recv())

    assert ws2_resp['model'] == 'profile'
    assert ws2_resp['data']['id'] == profile_id
    assert ws2_resp['data']['dateModified'] == patch_resp['data']['dateModified']

    ws_resp = loads(ws.recv())

    assert ws_resp['model'] == 'profile'
    assert ws_resp['data']['id'] == profile_id
    assert ws_resp['data']['dateModified'] == patch_resp['data']['dateModified']

    ws.close()

    test_product = api.load('test_product')
    product_id = test_product['data']['id']
    product_patch = {"data": {"title": "Тест змін продукту"}}
    product_patch['access'] = dict(test_product['access'])

    resp = api.patch('products/%s' % product_id, json=product_patch)
    assert resp.status_code == 200
    patch_resp = resp.json()
    sleep(0.1)
    ws2_resp = loads(ws2.recv())

    assert ws2_resp['model'] == 'product'
    assert ws2_resp['data']['id'] == product_id
    assert ws2_resp['data']['dateModified'] == patch_resp['data']['dateModified']

    test_offer = api.load('test_offer')
    offer_id = api.load('offer_id')
    offer_patch = {"data": {"comment": "Тест змін пропозиції"}}
    offer_patch['access'] = dict(test_offer['access'])

    resp = api.patch('offers/%s' % offer_id, json=offer_patch)
    assert resp.status_code == 200
    patch_resp = resp.json()
    sleep(0.1)
    ws2_resp = loads(ws2.recv())

    assert ws2_resp['model'] == 'offer'
    assert ws2_resp['data']['id'] == offer_id
    assert ws2_resp['data']['dateModified'] == patch_resp['data']['dateModified']

    resp = api.patch('offers/%s' % offer_id, json=offer_patch)
    assert resp.status_code == 200
    # close w/o recv
    ws2.close()
    sleep(0.1)

    resp = api.patch('offers/%s' % offer_id, json=offer_patch)
    assert resp.status_code == 200
    sleep(0.1)

    ws3 = create_connection(changes_url, timeout=1)

    resp = api.patch('offers/%s' % offer_id, json=offer_patch)
    assert resp.status_code == 200
    patch_resp = resp.json()
    sleep(0.1)
    ws3_resp = loads(ws3.recv())

    assert ws3_resp['model'] == 'offer'
    assert ws3_resp['data']['id'] == offer_id
    assert ws3_resp['data']['dateModified'] == patch_resp['data']['dateModified']

    ws3.close()
