import os
if 'CATALOG_CHANGES' in os.environ:
    from gevent import monkey, sleep
    monkey.patch_all()
else:
    from time import sleep


def api_method(method):
    def newfunc(self, url, *args, **kwargs):
        return self.session.request(method, self.base_url + url, *args, **kwargs)
    return newfunc


def server_thread(app, host, port):
    app.use_debugger = False
    app.use_reloader = False
    if 'CATALOG_CHANGES' in os.environ:
        from prozorro.catalog import gevent_wsgi_serve_forever
        gevent_wsgi_serve_forever(app, host, port)
    else:
        app.run(host=host, port=port)


class ApiTester(object):
    base_url = 'http://127.0.0.1:5000/api/0/'
    cache_map = dict()

    def __init__(self):
        import requests
        self.session = requests.Session()
        if 'TEST_BASE_URL' in os.environ:
            self.base_url = os.environ['TEST_BASE_URL']
        if 'TEST_BASIC_AUTH' in os.environ:
            self.session.auth = tuple(os.environ['TEST_BASIC_AUTH'].split(':'))
        if 'CATALOG_CREATE' in os.environ:
            from prozorro.catalog import create_catalog_database
            create_catalog_database(create_test_user=True)
        if 'TEST_RUN_SERVER' in os.environ:
            self.start_server()

    head = api_method('head')
    get = api_method('get')
    put = api_method('put')
    post = api_method('post')
    patch = api_method('patch')
    delete = api_method('delete')

    def abs_get(self, url, *args, **kwargs):
        end = self.base_url.find('/', 10)
        return self.session.get(self.base_url[:end] + url, *args, **kwargs)

    def save(self, name, obj):
        self.cache_map[name] = obj

    def load(self, name):
        return self.cache_map[name]

    def start_server(self):
        import threading
        from random import randint
        from requests.exceptions import ConnectionError
        from prozorro.catalog import create_app
        self.app = create_app()
        host, port = '127.0.0.1', randint(8000, 9000)
        self.base_url = 'http://{}:{}/api/0/'.format(host, port)
        self.server_thread = threading.Thread(target=server_thread,
            args=(self.app, host, port), daemon=True)
        self.server_thread.start()
        # wait for server
        for _ in range(10):
            try:
                self.get('schemas')
            except ConnectionError:
                sleep(0.1)
                continue
            break


api = ApiTester()
