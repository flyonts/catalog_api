from . import api


def test_050_health():
    resp = api.abs_get('/')
    assert resp.status_code == 200
    assert resp.json()['status'] == 'ok'


def test_051_schema_list():
    resp = api.get('schemas')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0

    schema_id = resp_json['data'][0]['id']

    resp = api.get('schemas/%s' % schema_id)
    assert resp.status_code == 200
    assert '$schema' in resp.text

    schema_id += 'non_exists'
    resp = api.get('schemas/%s' % schema_id)
    assert resp.status_code == 404


def test_052_schema_post():
    resp = api.post('schemas')
    assert resp.status_code == 405

    resp = api.put('schemas/schema_id')
    assert resp.status_code == 405
