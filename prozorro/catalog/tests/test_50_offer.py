from . import api
from copy import deepcopy
from urllib.parse import quote
from ..utils import get_hsah_id


test_offer = {
  "data": {
    "comment": "Доставка тільки по Києву",
    "deliveryAddresses": [
      {
        "countryName": "Україна",
        "locality": "Київ",
        "postalCode": "01001",
        "region": "Київ"
      }
    ],
    "status": "active",
    "suppliers": [
      {
        "address": {
          "countryName": "Україна"
        },
        "contactPoint": {
          "email": "K2.TENDER@EPICENTR.COM",
          "name": "Векленко Ольга Валеріївна",
          "telephone": "0638771741",
          "url": "https://epicentr.ua"
        },
        "identifier": {
          "id": "32490244",
          "legalName": "ТОВ \"Епіцентр\"",
          "scheme": "UA-EDR"
        },
        "name": "ТОВ \"Епіцентр\""
      }
    ],
    "minOrderValue": {
      "amount": 350,
      "currency": "UAH"
    },
    "value": {
      "amount": 35,
      "currency": "UAH",
      "valueAddedTaxIncluded": True
    }
  }
}

patch_offer_bad = {
  "data": {
    "id": "unknown"
  }
}

patch_offer = {
  "data": {
    "comment": "Доставка тільки по Києву на наступний день",
    "value": {
      "amount": 33,
      "currency": "UAH",
      "valueAddedTaxIncluded": True
    }
  }
}

patch_offer_address = {
  "data": {
    "comment": "Доставка не тільки по Києву",
    "suppliers": [
      {
        "contactPoint": {
          "email": "K3@EPICENTR.COM",
          "name": "Сергій Тестеров",
          "telephone": "0995553322",
          "url": "https://epicentr.ua"
        }
      }
    ]
  }
}


def test_510_offer_create():
    test_product = api.load('test_product')
    product_id = test_product['data']['id']

    test_offer['data']['relatedProduct'] = product_id

    offer_id = get_hsah_id(test_offer['data'])

    resp = api.patch('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 404

    test_offer['data']['value']['currency'] = 'USD'

    offer_id = get_hsah_id(test_offer['data'])
    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'value.currency mismatch' in resp.text

    test_offer['data']['value']['currency'] = 'UAH'

    test_offer['data']['minOrderValue']['amount'] = 1

    offer_id = get_hsah_id(test_offer['data'])
    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'minOrderValue.amount mismatch' in resp.text

    test_offer['data']['minOrderValue']['amount'] = 500
    test_offer['data']['minOrderValue']['currency'] = 'USD'

    offer_id = get_hsah_id(test_offer['data'])
    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'minOrderValue.currency mismatch' in resp.text

    test_offer['data']['minOrderValue']['currency'] = 'UAH'

    offer_id = get_hsah_id(test_offer['data'])
    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 201
    resp_json = resp.json()
    assert resp_json['data']['id'] == offer_id
    assert 'access' in resp_json
    assert 'token' in resp_json['access']

    test_dateModified = resp_json['data']['dateModified']

    test_offer_copy = deepcopy(test_offer)
    test_offer['access'] = resp_json['access']

    resp = api.put('offers/%s' % offer_id, json=test_offer_copy)
    assert resp.status_code == 400

    resp = api.get('offers')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0
    assert set(resp_json['data'][0].keys()) == set(['id', 'dateModified'])
    assert offer_id in [i['id'] for i in resp_json['data']]
    items = [i for i in resp_json['data'] if i['id'] == offer_id]
    assert len(items) == 1
    assert items[0]['dateModified'] == test_dateModified

    resp = api.get('offers?offset=' + quote(resp_json['data'][-1]['dateModified']))
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 0

    resp = api.get('offers/%s' % offer_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['id'] == offer_id

    test_offer['data']['id'] = offer_id
    api.save('test_offer', test_offer)
    api.save('offer_id', offer_id)


def test_520_offer_invalid():
    test_offer = api.load('test_offer')
    test_product = api.load('test_product')
    product_id = test_product['data']['id']

    offer_id = get_hsah_id(test_offer['data'])

    test_offer['data']['relatedProduct'] = product_id
    test_offer['data']['dateModified'] = '2019-01-01T20:20:20.000+02:00'

    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'dateModified' in resp.text

    test_offer['data']['comment'] += "."
    test_offer['data'].pop('dateModified')
    test_offer['data'].pop('id')

    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'hash id mismatch' in resp.text

    test_offer['data']['relatedProduct'] = product_id + '0'

    offer_id = get_hsah_id(test_offer['data'])

    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'relatedProduct' in resp.text

    test_offer['data']['relatedProduct'] = product_id
    test_offer['data']['value']['amount'] += 10000

    offer_id = get_hsah_id(test_offer['data'])

    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'value.amount' in resp.text

    test_offer['data']['value']['amount'] -= 10000
    test_offer['data'].pop('status')

    offer_id = get_hsah_id(test_offer['data'])

    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert "'status' is a required property" in resp.text

    test_offer['data']['status'] = 'active'
    test_offer['data']['suppliers'][0]['identifier']['id'] += '0'

    offer_id = get_hsah_id(test_offer['data'])

    resp = api.put('offers/%s' % offer_id, json=test_offer)
    assert resp.status_code == 400
    assert 'supplier not found' in resp.text


def test_530_offer_patch():
    test_offer = api.load('test_offer')
    offer_id = api.load('offer_id')

    test_offer['data']['id'] = offer_id

    resp = api.get('offers/%s' % offer_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert offer_id == resp_json['data']['id']

    resp = api.patch('offers/%s' % offer_id, json=patch_offer_bad)
    assert resp.status_code == 401

    patch_offer_bad['access'] = {'token': 'bad access token, but long enough'}

    resp = api.patch('offers/%s' % offer_id, json=patch_offer_bad)
    assert resp.status_code == 403

    patch_offer_bad['access'] = dict(test_offer['access'])

    resp = api.patch('offers/%s' % offer_id, json=patch_offer_bad)
    assert resp.status_code == 400

    patch_offer['access'] = dict(test_offer['access'])

    resp = api.patch('offers/%s' % offer_id, json=patch_offer)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_offer['data'].items():
        assert str(resp_json['data'][key]) == str(patch_value)

    test_dateModified = resp_json['data']['dateModified']

    resp = api.get('offers')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0
    items = [i for i in resp_json['data'] if i['id'] == offer_id]
    assert len(items) == 1
    assert items[0]['dateModified'] == test_dateModified

    resp = api.get('offers/%s' % offer_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_offer['data'].items():
        assert str(resp_json['data'][key]) == str(patch_value)

    patch_offer_address['access'] = dict(test_offer['access'])

    patch_offer_address["data"]["suppliers"][0]["address"] = {
        "countryName": "Україна"
    }

    resp = api.patch('offers/%s' % offer_id, json=patch_offer_address)
    assert resp.status_code == 400
    assert 'address' in resp.text

    patch_offer_address["data"]["suppliers"][0].pop("address")

    resp = api.patch('offers/%s' % offer_id, json=patch_offer_address)
    assert resp.status_code == 200
    assert patch_offer_address["data"]["suppliers"][0]["contactPoint"]["name"] == \
        patch_offer_address["data"]["suppliers"][0]["contactPoint"]["name"]


def test_540_offer_limit_offset():
    test_offer = api.load('test_offer')
    test_product = api.load('test_product')
    product_id = test_product['data']['id']

    test_product = api.load('test_product')
    product_id = test_product['data']['id']

    test_offer_map = dict()

    resp = api.get('offers')
    assert resp.status_code == 200
    resp_json = resp.json()
    for item in resp_json['data']:
        test_offer_map[item['id']] = item['dateModified']

    for i in range(10):
        test_offer_copy = deepcopy(test_offer)
        test_offer_copy['data']['relatedProduct'] = product_id
        test_offer_copy['data']['suppliers'][0]['identifier']['id'] = "32490244"
        test_offer_copy['data']['comment'] += " copy {}".format(i + 1)
        test_offer_copy['data'].pop('id', '')
        test_offer_copy['data'].pop('dateModified', '')
        offer_id = get_hsah_id(test_offer_copy['data'])

        resp = api.put('offers/%s' % offer_id, json=test_offer_copy)
        assert resp.status_code == 201
        resp_json = resp.json()
        assert resp_json['data']['id'] == offer_id
        assert 'access' in resp_json
        assert 'token' in resp_json['access']

        test_dateModified = resp_json['data']['dateModified']
        test_offer_map[offer_id] = test_dateModified

    resp = api.get('offers?reverse=1')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 10
    prev = resp_json['data'][0]
    assert test_offer_map[prev['id']] == prev['dateModified']
    for item in resp_json['data'][1:]:
        assert prev['dateModified'] > item['dateModified']
        assert test_offer_map[item['id']] == item['dateModified']

    offset = ''
    while True:
        resp = api.get('offers?limit=5&offset=' + quote(offset))
        assert resp.status_code == 200
        resp_json = resp.json()
        if len(resp_json['data']) == 0:
            assert 'next_page' not in resp_json
            break
        assert 'next_page' in resp_json
        assert 'offset' in resp_json['next_page']
        offset = resp_json['next_page']['offset']

        assert len(resp_json['data']) <= 5
        prev = resp_json['data'][0]
        assert test_offer_map.pop(prev['id']) == prev['dateModified']
        for item in resp_json['data'][1:]:
            assert prev['dateModified'] < item['dateModified']
            assert test_offer_map.pop(item['id']) == item['dateModified']

    assert len(test_offer_map) == 0
