from . import api
from random import randint
from copy import deepcopy
from urllib.parse import quote


test_product = {
  "data": {
    "additionalClassifications": [
      {
        "description": "Засоби індивідуального захисту (респіратори та маски) без клапану",
        "id": "5011020",
        "scheme": "KMU777"
      }
    ],
    "additionalProperties": [
      {
        "code": "ocds-color",
        "name": "Колір",
        "value": "білий"
      },
      {
        "code": "ocds-warranty",
        "name": "гарантія від виробника",
        "value": True
      }
    ],
    "alternativeIdentifiers": [
      {
        "id": "0463234567819",
        "scheme": "EAN-13"
      }
    ],
    "brand": {
      "name": "IGAR",
      "uri": "igar.ua"
    },
    "classification": {
      "description": "Медичне обладнання та вироби медичного призначення різні",
      "id": "33190000-8",
      "scheme": "ДК021"
    },
    "description": "Маски медичні IGAR тришарові на гумках 50 шт./уп. Гарантія від виробника.",
    "identifier": {
      "id": "463234567819",
      "scheme": "UPC"
    },
    "images": [
      {
        "sizes": "500x175",
        "url": "https://catalog.openprocurement.org.ua/images/619890.500x175.jpeg"
      }
    ],
    "manufacturers": [
      {
        "address": {
          "countryName": "Україна",
          "locality": "м.Київ",
          "postalCode": "01024",
          "region": "Київ",
          "streetAddress": "вул. Приблизна 1, копус 20"
        },
        "contactPoint": {
          "email": "igar@igar.ua",
          "name": "Анакун Анастасия",
          "telephone": "+380442342414"
        },
        "identifier": {
          "id": "39454432",
          "legalName": "ТОВ \"Ігар\"",
          "scheme": "UA-EDR"
        },
        "name": "ТОВ \"ІГАР\""
      }
    ],
    "product": {
      "name": "Маска тришарова на гумках"
    },
    "requirementResponses": [
      {
        "id": "medic-use-type",
        "requirement": "0001-001-01",
        "value": "одноразова"
      },
      {
        "id": "medic-layers",
        "requirement": "0002-001-01",
        "value": 3
      },
      {
        "id": "packing-count",
        "requirement": "0003-001-01",
        "value": 50
      },
      {
        "id": "nose-clip",
        "requirement": "0004-001-01",
        "value": True
      },
      {
        "id": "test-allOf",
        "requirement": "0005-001-01",
        "value": ["ALL_A", "ALL_B", "ALL_C"]
      },
      {
        "id": "test-anyOf",
        "requirement": "0005-002-01",
        "value": ["ANY_A", "ANY_C"]
      },
      {
        "id": "test-oneOf",
        "requirement": "0005-003-01",
        "value": "ONE_B"
      }
    ],
    "status": "active",
    "title": "Маски медичні IGAR тришарові на гумках 50 шт./уп."
  }
}

patch_product_bad = {
  "data": {
    "status": "unknown"
  }
}

patch_product_identifier = {
  "data": {
    "identifier": {
      "id": "0463234567819",
      "scheme": "EAN-13"
    }
  }
}

patch_product = {
  "data": {
    "status": "hidden",
    "title": "Маски (приховані)"
  }
}


def test_410_product_create():
    test_profile = api.load('test_profile')
    profile_id = test_profile['data']['id']
    cpv = test_product['data']['classification']['id']
    test_product['data']['classification']['id'] = '12345678'

    product_id = '{}-{}-{}-{}'.format(
        test_product['data']['classification']['id'][:4],
        test_product['data']['brand']['name'][:4],
        test_product['data']['identifier']['id'][:13],
        randint(100000, 900000))

    test_product['data']['id'] = product_id
    test_product['data']['relatedProfile'] = profile_id
    test_product['access'] = dict(test_profile['access'])

    resp = api.patch('products/%s' % product_id, json=test_product)
    assert resp.status_code == 404

    resp = api.put('products/%s' % product_id, json=test_product)
    assert resp.status_code == 400
    assert 'classification mismatch' in resp.text

    test_product['data']['classification']['id'] = '87654321'

    resp = api.put('products/%s' % product_id, json=test_product)
    assert resp.status_code == 400
    assert 'id must include classification' in resp.text

    test_product['data']['classification']['id'] = cpv

    product_id = '{}-{}-{}-{}'.format(
        test_product['data']['classification']['id'][:4],
        test_product['data']['brand']['name'][:4],
        test_product['data']['identifier']['id'][:13],
        randint(100000, 900000))
    test_product['data']['id'] = product_id

    resp = api.put('products/%s' % product_id + '-1', json=test_product)
    assert resp.status_code == 400
    assert 'id mismatch' in resp.text

    resp = api.put('products/%s' % product_id, json=test_product)
    assert resp.status_code == 201
    resp_json = resp.json()
    assert resp_json['data']['id'] == test_product['data']['id']
    assert 'access' in resp_json
    assert 'token' in resp_json['access']

    test_product_copy = deepcopy(test_product)
    test_product['access'] = resp_json['access']

    resp = api.put('products/%s' % product_id, json=test_product_copy)
    assert resp.status_code == 400
    assert 'Already exists' in resp.text

    product_id_copy = product_id + '1'
    test_product_copy['data']['id'] = product_id_copy
    for i in range(3):
        test_product_copy['data']['requirementResponses'].pop()

    resp = api.put('products/%s' % product_id_copy, json=test_product_copy)
    assert resp.status_code == 400
    assert 'criteria 0005 not satisfied' in resp.text

    resp = api.get('products')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 0
    assert set(resp_json['data'][0].keys()) == set(['id', 'dateModified'])
    assert product_id in [i['id'] for i in resp_json['data']]

    resp = api.get('products/%s' % product_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['id'] == test_product['data']['id']

    api.save('test_product', test_product)


def test_420_product_patch():
    test_product = api.load('test_product')
    product_id = test_product['data']['id']

    resp = api.get('products/%s' % product_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert product_id == resp_json['data']['id']

    resp = api.patch('products/%s' % product_id, json=patch_product_bad)
    assert resp.status_code == 401

    patch_product_bad['access'] = {'token': 'bad access token, but long enough'}

    resp = api.patch('products/%s' % product_id, json=patch_product_bad)
    assert resp.status_code == 403

    patch_product_bad['access'] = dict(test_product['access'])

    resp = api.patch('products/%s' % product_id, json=patch_product_bad)
    assert resp.status_code == 400

    patch_product_identifier['access'] = dict(test_product['access'])

    resp = api.patch('products/%s' % product_id, json=patch_product_identifier)
    assert resp.status_code == 400

    patch_product['access'] = dict(test_product['access'])

    resp = api.patch('products/%s' % product_id, json=patch_product)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_product['data'].items():
        assert resp_json['data'][key] == patch_value

    resp = api.get('products/%s' % product_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_product['data'].items():
        assert resp_json['data'][key] == patch_value

    patch_product['data']['status'] = 'active'
    resp = api.patch('products/%s' % product_id, json=patch_product)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['status'] == 'active'


def test_430_product_limit_offset():
    test_profile = api.load('test_profile')
    profile_id = test_profile['data']['id']
    test_product = api.load('test_product')
    product_id = test_product['data']['id']

    test_product_map = dict()
    resp = api.get('products')
    assert resp.status_code == 200
    resp_json = resp.json()
    for item in resp_json['data']:
        test_product_map[item['id']] = item['dateModified']

    for i in range(10):
        product_id = '{}-{}-{}-{}'.format(
            test_product['data']['classification']['id'][:4],
            test_product['data']['brand']['name'][:4],
            test_product['data']['identifier']['id'][:13],
            randint(100000, 900000))

        test_product_copy = deepcopy(test_product)
        test_product_copy['data']['id'] = product_id
        test_product_copy['data']['relatedProfile'] = profile_id
        test_product_copy['access'] = dict(test_profile['access'])

        resp = api.put('products/%s' % product_id, json=test_product_copy)
        assert resp.status_code == 201
        resp_json = resp.json()
        assert resp_json['data']['id'] == test_product_copy['data']['id']
        assert 'access' in resp_json
        assert 'token' in resp_json['access']

        test_product_map[product_id] = resp_json['data']['dateModified']

    resp = api.get('products?reverse=1')
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) > 10
    prev = resp_json['data'][0]
    assert test_product_map[prev['id']] == prev['dateModified']
    for item in resp_json['data'][1:]:
        assert prev['dateModified'] > item['dateModified']
        assert test_product_map[item['id']] == item['dateModified']

    offset = ''
    while True:
        resp = api.get('products?limit=5&offset=' + quote(offset))
        assert resp.status_code == 200
        resp_json = resp.json()
        if len(resp_json['data']) == 0:
            assert 'next_page' not in resp_json
            break
        assert 'next_page' in resp_json
        assert 'offset' in resp_json['next_page']
        offset = resp_json['next_page']['offset']

        assert len(resp_json['data']) <= 5
        prev = resp_json['data'][0]
        assert test_product_map.pop(prev['id']) == prev['dateModified']
        for item in resp_json['data'][1:]:
            assert prev['dateModified'] < item['dateModified']
            assert test_product_map.pop(item['id']) == item['dateModified']

    assert len(test_product_map) == 0
