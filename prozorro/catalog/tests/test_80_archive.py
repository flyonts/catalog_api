from . import api
from os import environ, path
from prozorro.catalog.catalog import Catalog
from ..utils import get_now


def test_800_verify_archive():
    if 'CATALOG_DATA' not in environ:   # pragma: no cover
        return
    catalog = Catalog(environ['CATALOG_DATA'])
    test_category = api.load('test_category')
    test_profile = api.load('test_profile')
    test_product = api.load('test_product')
    test_offer = api.load('test_offer')

    arcpath = path.join('archive', get_now().isoformat()[:7])
    objects = catalog.list_objects(arcpath)
    listids = [i['id'].split('_')[0] for i in objects]

    assert len(objects) > 10
    assert test_category['data']['id'] in listids
    assert test_profile['data']['id'] in listids
    assert test_product['data']['id'] in listids
    assert test_offer['data']['id'] in listids
