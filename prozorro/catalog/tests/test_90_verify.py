from os import environ
from prozorro.catalog import verify_catalog_database


def test_900_verify_catalog_database():
    if 'CATALOG_DATA' not in environ:  # pragma: no cover
        return
    result = dict()
    verify_catalog_database(result=result)
    assert result['category'] == 11
    assert result['profile'] == 11
    assert result['product'] == 11
    assert result['offer'] == 11
