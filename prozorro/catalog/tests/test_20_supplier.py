from . import api
from random import randint


test_supplier = {
  "data": {
    "id": "32490244",
    "address": {
      "countryName": "Україна",
      "region": "Київ",
      "locality": "Київ",
      "postalCode": "01003",
      "streetAddress": "невідомо"
    },
    "contactPoint": {
      "email": "k2.tender@epicent.com",
      "name": "Векленко Ольга Валеріївна",
      "telephone": "0638771741",
      "url": "https://epicentr.ua"
    },
    "identifier": {
      "id": "32490244",
      "legalName": "ТОВ \"Епіцентр\"",
      "scheme": "UA-EDR"
    },
    "name": "ТОВ \"Епіцентр\"",
    "scale": "sme",
    "status": "active"
  }
}

patch_supplier = {
  "data": {
    "name": "ТОВ \"Епіцентр\" (призупинений)",
    "status": "suspended"
  }
}


def test_210_supplier_create():
    test_category = api.load('test_category')

    category_id = test_category['data']['id']
    supplier_id = test_supplier['data']['id']

    resp = api.get('categories/%s/suppliers' % category_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 0

    resp = api.patch('categories/%s/suppliers/%s' % (category_id, supplier_id), json=test_supplier)
    assert resp.status_code == 401

    test_supplier['access'] = dict(test_category['access'])

    resp = api.patch('categories/%s/suppliers/%s' % (category_id, supplier_id), json=test_supplier)
    assert resp.status_code == 404

    resp = api.put('categories/%s/suppliers/%s' % (category_id, supplier_id + '-1'), json=test_supplier)
    assert resp.status_code == 400
    assert 'id mismatch' in resp.text

    test_supplier['data']['id'] = supplier_id = test_supplier['data']['identifier']['id']

    resp = api.put('categories/%s/suppliers/%s' % (category_id, supplier_id), json=test_supplier)
    assert resp.status_code == 400
    assert 'bad format' in resp.text

    test_supplier['data']['id'] = supplier_id = "{}-{}".format(
        test_supplier['data']['identifier']['scheme'],
        test_supplier['data']['identifier']['id'])

    resp = api.put('categories/%s/suppliers/%s' % (category_id, supplier_id), json=test_supplier)
    assert resp.status_code == 201
    resp_json = resp.json()
    assert resp_json['data']['id'] == test_supplier['data']['id']

    resp = api.put('categories/%s/suppliers/%s' % (category_id, supplier_id), json=test_supplier)
    assert resp.status_code == 400
    assert 'already exists' in resp.text

    resp = api.get('categories/%s/suppliers' % category_id)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert len(resp_json['data']) == 1

    resp = api.get('categories/%s/suppliers/%s' % (category_id, supplier_id))
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['id'] == test_supplier['data']['id']


def test_220_supplier_patch():
    test_category = api.load('test_category')

    category_id = test_category['data']['id']
    supplier_id = test_supplier['data']['id']

    resp = api.patch('categories/%s/suppliers/%s' % (category_id, supplier_id), json=patch_supplier)
    assert resp.status_code == 401

    patch_supplier['access'] = dict(test_category['access'])

    resp = api.patch('categories/%s/suppliers/%s' % (category_id, supplier_id), json=patch_supplier)
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_supplier['data'].items():
        assert resp_json['data'][key] == patch_value

    resp = api.get('categories/%s/suppliers/%s' % (category_id, supplier_id))
    assert resp.status_code == 200
    resp_json = resp.json()
    for key, patch_value in patch_supplier['data'].items():
        assert resp_json['data'][key] == patch_value


def test_230_supplier_delete():
    test_category = api.load('test_category')

    category_id = test_category['data']['id']
    supplier_id = test_supplier['data']['id']
    token = test_category['access']['token']

    resp = api.delete('categories/%s/suppliers/%s' % (category_id, supplier_id))
    assert resp.status_code == 401

    resp = api.delete('categories/%s/suppliers/%s?access_token=%s' % (category_id, supplier_id, token))
    assert resp.status_code == 200

    resp = api.get('categories/%s/suppliers/%s' % (category_id, supplier_id))
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['status'] == 'deleted'

    patch_supplier['access'] = dict(test_category['access'])
    patch_supplier['data']['status'] = 'active'

    resp = api.patch('categories/%s/suppliers/%s' % (category_id, supplier_id), json=patch_supplier)
    assert resp.status_code == 200
    resp_json = resp.json()
    assert resp_json['data']['status'] == 'active'
