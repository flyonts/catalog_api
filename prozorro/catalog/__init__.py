import os
if 'GEVENT_MONKEY_PATCH' in os.environ:  # noqa
    from gevent import monkey
    monkey.patch_all()


__version__ = '0'


class HTTPMethodOverrideMiddleware(object):
    allow_override_from = frozenset([
        'POST',
    ])
    allow_override_to = frozenset([
        'PATCH',
        'PUT',
        'DELETE',
    ])

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        if environ['REQUEST_METHOD'] in self.allow_override_from:
            method = environ.get('HTTP_X_HTTP_METHOD_OVERRIDE', '')
            if method in self.allow_override_to:
                environ['REQUEST_METHOD'] = method
            elif method:
                allowed_methods = ', '.join(self.allow_override_to)
                start_response('405 Method Not Allowed', [('Allow', allowed_methods)])
                return ["Error 405"]
        return self.app(environ, start_response)


def register_api(app, view):
    view_func = view.as_view(view.__name__)
    collection_url = '/api/{}/{}'.format(__version__, view.path)
    object_url = '{}/<{}>'.format(collection_url, view.pk)
    app.add_url_rule(collection_url, view_func=view_func, methods=['GET'])
    app.add_url_rule(collection_url, view_func=view_func, methods=['POST'])
    app.add_url_rule(object_url, view_func=view_func, methods=['GET', 'POST',
                     'PATCH', 'PUT', 'DELETE'])


def create_app(config=None):
    from flask import Flask
    from .catalog import Catalog
    app = Flask(__name__)
    app.config.update({
        'ARCHIVE': True,
        'CACHE': False,
        'CHANGES': False,
        'PROXY_FIX': False,
        'METHOD_OVERRIDE': False,
    })
    for key, value in os.environ.items():
        if key.startswith('CATALOG_'):
            app.config[key[8:]] = value
    if app.config['PROXY_FIX']:
        from werkzeug.contrib.fixers import ProxyFix
        app.wsgi_app = ProxyFix(app.wsgi_app)
    if app.config['METHOD_OVERRIDE']:
        app.wsgi_app = HTTPMethodOverrideMiddleware(app.wsgi_app)

    cache = app.config['CACHE']
    archive = app.config['ARCHIVE']
    app.catalog = Catalog(app.config['DATA'], archive=archive, cache=cache, logger=app.logger)

    from .views import health, category, profile, product, offer, schema, image
    app.add_url_rule('/', view_func=health.health_view)
    register_api(app, category.CategoryAPI)
    register_api(app, category.CategorySupplierAPI)
    register_api(app, profile.ProfileAPI)
    register_api(app, product.ProductAPI)
    register_api(app, offer.OfferAPI)
    register_api(app, schema.SchemaAPI)
    register_api(app, image.ImageAPI)

    if app.config['CHANGES']:
        from .views import changes
        changes_url = '/api/{}/changes'.format(__version__)
        changes.init_app(app, changes_url)

    return app


def create_catalog_database(create_test_user=False):
    data = os.environ['CATALOG_DATA']
    for name in ['archive', 'category', 'offer', 'product', 'profile', 'image', 'schema', 'tmp']:
        path = os.path.join(data, name)
        os.makedirs(path, 0o700, exist_ok=True)
    path = os.path.join(data, 'static/images')
    os.makedirs(path, 0o751, exist_ok=True)
    os.chmod(data, 0o751)
    # create auth.ini
    auth = os.path.join(data, 'auth.ini')
    with open(auth, 'a+t') as fp:
        fp.seek(0)
        text = fp.read()
        for sect in ['category', 'offer', 'product', 'profile', 'image']:
            if sect not in text:
                fp.write("\n[{}]\n".format(sect))
                if create_test_user:
                    fp.write("test\n\n")
    os.chmod(auth, 0o600)


def verify_catalog_database(real_save=False, result=None, logger=None):
    if not logger:
        import logging
        logformat = '%(asctime)s %(levelname)s %(message)s'
        logging.basicConfig(format=logformat, level=logging.INFO)
        logger = logging.getLogger(__name__)
    from .catalog import Catalog
    cache = os.environ.get('CATALOG_CACHE', False)
    catalog = Catalog(os.environ['CATALOG_DATA'], cache=cache, logger=logger)
    validated = catalog.verify_database(not real_save)
    if isinstance(result, dict):
        result.update(validated)
    logger.info("Total {}".format(validated))


def gevent_wsgi_serve_forever(app, host, port):
    from gevent.pywsgi import WSGIServer
    from geventwebsocket.handler import WebSocketHandler
    app.logger.info(" * Running on http://{}:{}".format(host, port))
    app.logger.info(" * Debug mode {}".format(app.debug))
    server = WSGIServer((host, port), app, handler_class=WebSocketHandler)
    server.serve_forever()


def run_as_websocket_server(setup_logging=True):    # pragma: no cover
    if setup_logging:
        import logging
        logging.basicConfig(format='%(message)s', level=logging.INFO)
    import sys
    from functools import partial
    from werkzeug.debug import DebuggedApplication
    from werkzeug.serving import run_with_reloader
    host = sys.argv[1] if len(sys.argv) > 1 else '0.0.0.0'
    port = int(sys.argv[2]) if len(sys.argv) > 2 else 5000
    app = create_app()
    if app.debug:
        app.use_debugger = True
        app.use_reloader = True
        app.wsgi_app = DebuggedApplication(app.wsgi_app, evalex=True)
        serve_forever = partial(gevent_wsgi_serve_forever, app=app, host=host, port=port)
        run_with_reloader(serve_forever)
    else:
        gevent_wsgi_serve_forever(app, host, port)
