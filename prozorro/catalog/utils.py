from os import environ
from flask import abort, current_app, request
from hashlib import sha256
from rapidjson import dumps
from datetime import datetime
from dateutil import tz
from secrets import token_hex, compare_digest

TZ = tz.gettz(environ.get('TZ', 'Europe/Kiev'))


def get_now(tz=TZ):
    return datetime.now(tz=tz)


def fromtimestamp(ts, tz=TZ):
    return datetime.fromtimestamp(ts).replace(tzinfo=tz)


def get_object_or_404(path, object_id):
    try:
        return current_app.catalog.read_object(path, object_id)
    except current_app.catalog.NotFound:
        abort(404, '{} {} not found'.format(path, object_id))


def get_access_token():
    if 'X-Access-Token' in request.headers:
        return request.headers['X-Access-Token']
    elif 'access_token' in request.args:
        return request.args['access_token']
    elif request.json and 'access' in request.json:
        return request.json['access'].get('token')
    return None


def hash_access_token(token):
    return sha256(token.encode()).hexdigest()


def validate_access_token(obj, validate_owner=True):
    token = get_access_token()
    if not token or len(token) < 32:
        abort(401, 'Require access token')
    if not compare_digest(hash_access_token(token), obj['access']['token']):
        abort(403, 'Access token mismatch')
    if validate_owner and get_owner() != obj['access']['owner']:
        abort(403, 'Owner mismatch')


def create_access_token():
    return token_hex(16)


def get_owner():
    auth = request.authorization
    if not auth:    # pragma: no cover
        return "anonymous"
    return auth.username


def set_access_token(obj, set_owner=True):
    obj['access'] = {'owner': get_owner(), 'token': create_access_token()}
    return obj


def get_hsah_id(obj):
    data = dumps(obj, ensure_ascii=False, sort_keys=True).encode('utf-8')
    return sha256(sha256(data).digest()).hexdigest()[:32]


def require_json_data(func):
    def inner_func(*args, **kwargs):
        if not request.json or 'data' not in request.json:
            abort(400, 'missed json[data] or header Content-Type: application/json')
        return func(*args, **kwargs)
    return inner_func
