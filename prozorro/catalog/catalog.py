import os
import re
import sys
import copy
import uuid
import fcntl
import rapidjson
import jsonschema
import configparser
from .utils import get_now, fromtimestamp, hash_access_token


class Catalog(object):
    NotFound = FileNotFoundError
    image_base = 'static/images'
    changes_callback = None
    fake_save = False
    cache = None

    def __init__(self, path, archive=True, cache=True, logger=None):
        self.path = os.path.abspath(path)
        self.archive = archive
        self.logger = logger
        self.load_authini()
        self.load_schemas()
        self.cache = dict() if cache else None

    def __enter__(self):
        lock_filename = os.path.join(self.path, '.lock')
        self.lock_file = open(lock_filename, 'w')
        fcntl.lockf(self.lock_file, fcntl.LOCK_EX)
        return self

    def __exit__(self, *args):
        fcntl.lockf(self.lock_file, fcntl.LOCK_UN)
        self.lock_file.close()
        self.lock_file = None

    @staticmethod
    def isidentifier(value):
        value = value.replace('-', '').replace('_', '')
        return 4 < len(value) < 40 and value.encode('latin1') and value.isalnum()

    def cache_get(self, key, default=None):
        if self.cache and key in self.cache:
            cached = self.cache[key]
            cached[0] -= 1
            if cached[0] > 0:
                return copy.deepcopy(cached[1])
            self.cache.pop(key)
        return default

    def cache_set(self, key, obj, ttl=100):
        if self.cache is None:
            return
        obj = copy.deepcopy(obj)
        self.cache[key] = [ttl, obj]

    def cache_del(self, key):
        if self.cache is None:
            return
        self.cache.pop(key, None)

    def image_file_path(self, filename):
        return os.path.join(self.path, self.image_base, filename)

    def image_file_url(self, filename):
        return '/{}/{}'.format(self.image_base, filename)

    def list_objects(self, path, reverse=False, root=None, use_mtime=False):
        if root is None:
            root = self.path
        fullpath = os.path.join(root, path)
        objects = self.cache_get(fullpath)
        if not objects:
            objects = list()
            with os.scandir(fullpath) as diriter:
                for entry in diriter:
                    if use_mtime:
                        dateModified = fromtimestamp(entry.stat().st_mtime).isoformat()
                    else:
                        obj = self.read_object(path, entry.name, root=root, safe_object_id=True)
                        dateModified = obj['data']['dateModified']
                    objects.append(dict(id=entry.name, dateModified=dateModified))
            objects.sort(key=lambda x: x['dateModified'])
            self.cache_set(fullpath, objects)
        if reverse:
            objects.reverse()
        return objects

    def read_object(self, path, object_id, root=None, safe_object_id=False):
        if root is None:
            root = self.path
        if not self.isidentifier(object_id) and not safe_object_id:
            raise ValueError('Bad id format')
        filename = os.path.join(root, path, object_id)
        cached = self.cache_get(filename)
        if cached:
            return cached
        with open(filename) as fp:
            obj = rapidjson.load(fp)
        self.cache_set(filename, obj)
        return obj

    def list_schemas(self):
        objects = list()
        for key, schema in self.schema.items():
            objects.append(dict(id=key, dateModified=schema.get('dateModified')))
        return objects

    def load_authini(self, name='auth.ini'):
        auth_file = os.path.join(self.path, name)
        self.auth = configparser.ConfigParser(allow_no_value=True)
        self.auth.read(auth_file)

    def load_schemas(self, path='schema'):
        self.schema = dict()
        root = os.path.dirname(sys.modules[__name__].__file__)
        # load default schemas from package
        for obj in self.list_objects(path, root=root, use_mtime=True):
            name = obj['id']
            schema = self.read_object(path, name, root=root, safe_object_id=True)
            assert '$schema' in schema
            name = name.replace('.json', '')
            self.schema[name] = schema
        # load schemas from database
        for obj in self.list_objects(path, use_mtime=True):
            name = obj['id']
            schema = self.read_object(path, name, safe_object_id=True)
            assert '$schema' in schema
            name = name.replace('.json', '')
            self.schema[name] = schema

    def get_schema(self, schema_id):
        return self.schema.get(schema_id, None)

    def set_changes_callback(self, callback):
        self.changes_callback = callback

    def save_object(self, path, obj, create=False):
        if 'access' not in obj or 'owner' not in obj['access']:
            raise ValueError('Owner required')
        if obj['access']['owner'] not in self.auth[path]:
            raise PermissionError('Not permitted')
        if not self.isidentifier(obj['data']['id']):
            raise ValueError('Bad id format')
        # validate and save
        obj['data']['dateModified'] = get_now().isoformat()
        jsonschema.validate(obj['data'], self.schema[path])
        fullpath = os.path.join(self.path, path)
        filename = os.path.join(fullpath, obj['data']['id'])
        tmp_file = os.path.join(self.path, 'tmp', uuid.uuid4().hex)
        if create and os.path.exists(filename) or os.path.exists(tmp_file):
            raise ValueError('Already exists')
        self.cache_del(fullpath)
        self.cache_del(filename)
        if self.fake_save:
            return obj
        if create:
            access_token = obj['access']['token']
            obj['access']['token'] = hash_access_token(access_token)
        # write to temp file, then verify, then rename
        with open(tmp_file, 'x', encoding='utf-8') as fp:
            rapidjson.dump(obj, fp, ensure_ascii=False)
        with open(tmp_file) as fp:
            tmp = rapidjson.load(fp)
            assert tmp['data']['dateModified'] == obj['data']['dateModified']
        # save revision in archive
        if not create and self.archive:
            archpath = os.path.join(self.path, 'archive', obj['data']['dateModified'][:7])
            if not os.path.exists(archpath):
                os.makedirs(archpath, 0o700, exist_ok=True)
            archfile = os.path.join(archpath, '{id}_{dateModified}'.format(**obj['data']))
            try:
                os.link(filename, archfile)
            except OSError:
                os.rename(filename, archfile)  # noqa
        # save changes by atomic rename temp file
        os.rename(tmp_file, filename)
        if self.logger:
            op = "Create" if create else "Update"
            self.logger.info("{} {} {}".format(op, path, obj['data']['id']))
        if create:
            obj['access']['token'] = access_token
        if self.changes_callback:
            self.changes_callback(model=path, data=obj['data'])
        return obj

    def validate_category_patch(self, patch):
        jsonschema.validate(patch, self.schema['category.patch'])

    def validate_supplier_patch(self, patch):
        jsonschema.validate(patch, self.schema['supplier.patch'])

    def validate_category_data(self, data):
        category_id = data['id']
        if len(category_id) < 20:
            raise ValueError('category.id is too small')
        if data['classification']['id'][:8] not in category_id:
            raise ValueError('id must include cpv')
        if data['procuringEntity']['identifier']['id'] not in category_id:
            raise ValueError('id must include edr')
        suppliers = data.get('suppliers', [])
        sids = [s['id'] for s in suppliers]
        if sids and len(sids) != len(set(sids)):
            raise ValueError('not unique suppliers.id')

    def save_category(self, category, create=False):
        self.validate_category_data(category['data'])
        return self.save_object('category', category, create=create)

    def validate_profile_patch(self, patch):
        jsonschema.validate(patch, self.schema['profile.patch'])

    def validate_profile_schema(self, data):
        data['dateModified'] = get_now().isoformat()
        jsonschema.validate(data, self.schema['profile'])

    def validate_profile_data(self, data):
        profile_id = data['id']
        category_id = data['relatedCategory']
        if category_id not in profile_id:
            raise ValueError('id must include category')
        acs = data.get('additionalClassifications')
        if acs and len(acs) != len(set([i['scheme'] + i['id'] for i in acs])):
            raise ValueError('not unique additionalClassifications.id')
        crids = set()
        codes = set()
        rgids = set()
        reqids = set()
        for cr in data['criteria']:
            if cr['id'] in crids:
                raise ValueError('not unique criteria.id %s' % cr['id'])
            crids.add(cr['id'])

            if cr['code'] in codes:
                raise ValueError('not unique criteria.code %s' % cr['code'])
            codes.add(cr['code'])

            for rg in cr['requirementGroups']:
                if rg['id'] in rgids:
                    raise ValueError('not unique requirementGroups.id %s' % rg['id'])
                rgids.add(rg['id'])

                for req in rg['requirements']:
                    if req['id'] in reqids:
                        raise ValueError('not unique requirements.id %s' % req['id'])
                    reqids.add(req['id'])

    def save_profile(self, profile, create=False):
        self.validate_profile_schema(profile['data'])
        self.validate_profile_data(profile['data'])
        return self.save_object('profile', profile, create=create)

    def validate_product_patch(self, patch):
        jsonschema.validate(patch, self.schema['product.patch'])

    def validate_product_schema(self, data):
        data['dateModified'] = get_now().isoformat()
        jsonschema.validate(data, self.schema['product'])

    def validate_product_data(self, data):
        product_id = data['id']
        profile_id = data['relatedProfile']
        try:
            profile = self.read_object('profile', profile_id)
        except self.NotFound:
            raise ValueError('relatedProfile %s not found' % profile_id)
        rrsids = set()
        rrsreqs = set()
        reqmap = dict()
        rrsmap = dict()

        if data['identifier']['id'][:12] not in product_id:
            raise ValueError('id must include identifier')
        if data['classification']['id'][:4] not in product_id:
            raise ValueError('id must include classification')

        if data['classification']['id'][:4] != profile['data']['classification']['id'][:4]:
            raise ValueError('product and profile classification mismatch')

        for cr in profile['data']['criteria']:
            for rg in cr['requirementGroups']:
                for req in rg['requirements']:
                    reqmap[req['id']] = req

        for r in data['requirementResponses']:
            if r['id'] in rrsids:
                raise ValueError('not unique requirementResponses.id %s' % r['id'])
            rrsids.add(r['id'])

            if r['requirement'] in rrsreqs:
                raise ValueError('not unique requirement %s' % r['requirement'])
            rrsreqs.add(r['requirement'])

            key = r['requirement']
            value = r['value']
            rrsmap[key] = r
            if key not in reqmap:
                raise ValueError('requirement %s not found' % key)
            requirement = reqmap[key]

            if 'expectedValue' in requirement:
                if value != requirement['expectedValue']:
                    raise ValueError('requirement %s unexpected value' % key)
            if 'minValue' in requirement:
                if value < requirement['minValue']:
                    raise ValueError('requirement %s minValue' % key)
            if 'maxValue' in requirement:
                if value > requirement['maxValue']:
                    raise ValueError('requirement %s maxValue' % key)
            if 'pattern' in requirement:
                if not re.match(requirement['pattern'], str(value)):
                    raise ValueError('requirement %s pattern' % key)
            if 'allOf' in requirement:
                if set(value) != set(requirement['allOf']):
                    raise ValueError('requirement %s allOf' % key)
            if 'anyOf' in requirement:
                if isinstance(value, list):
                    if not set(value).issubset(set(requirement['anyOf'])):
                        raise ValueError('requirement %s anyOf' % key)
                elif value not in requirement['anyOf']:
                    raise ValueError('requirement %s anyOf' % key)
            if 'oneOf' in requirement:
                if value not in requirement['oneOf']:
                    raise ValueError('requirement %s oneOf' % key)

        for cr in profile['data']['criteria']:
            group_found = 0
            for rg in cr['requirementGroups']:
                requirement_found = 0
                for req in rg['requirements']:
                    if req['id'] in rrsmap:
                        requirement_found += 1
                if requirement_found == len(rg['requirements']):
                    group_found += 1
            if group_found == 0:
                raise ValueError('criteria %s not satisfied' % cr['id'])

    def save_product(self, product, create=False):
        self.validate_product_schema(product['data'])
        self.validate_product_data(product['data'])
        return self.save_object('product', product, create=create)

    def validate_offer_data(self, data):
        try:
            product_id = data['relatedProduct']
            product = self.read_object('product', product_id)
            profile_id = product['data']['relatedProfile']
            profile = self.read_object('profile', profile_id)
            category_id = profile['data']['relatedCategory']
            category = self.read_object('category', category_id)
        except self.NotFound:
            raise ValueError('relatedProduct or his parents not found')

        active_suppliers = [s['identifier']['id'] for s in category['data']['suppliers'] if s['status'] == 'active']

        supplier = data['suppliers'][0]
        if supplier['identifier']['id'] not in active_suppliers:
            raise ValueError('supplier not found in category')

        if data['value']['amount'] > profile['data']['value']['amount']:
            raise ValueError('value.amount mismatch')
        if data['value']['currency'] > profile['data']['value']['currency']:
            raise ValueError('value.currency mismatch')
        if 'valueAddedTaxIncluded' in profile['data']['value']:
            if data['value']['valueAddedTaxIncluded'] != profile['data']['value']['valueAddedTaxIncluded']:
                raise ValueError('value.valueAddedTaxIncluded mismatch')

        if 'minOrderValue' in data:
            if data['minOrderValue']['amount'] < data['value']['amount']:
                raise ValueError('minOrderValue.amount mismatch')
            if data['minOrderValue']['currency'] != data['value']['currency']:
                raise ValueError('minOrderValue.currency mismatch')

    def validate_offer_patch(self, data):
        jsonschema.validate(data, self.schema['offer.patch'])

    def save_offer(self, offer, create=False):
        self.validate_offer_data(offer['data'])
        return self.save_object('offer', offer, create=create)

    def verify_database(self, fake_save=True):
        self.fake_save = fake_save
        validated = dict(category=0, profile=0, product=0, offer=0)

        for c in self.list_objects('category'):
            self.logger.info("Validate category {}".format(c['id']))
            category = self.read_object('category', c['id'])
            self.save_category(category)
            validated['category'] += 1

        for p in self.list_objects('profile'):
            self.logger.info("Validate profile {}".format(p['id']))
            profile = self.read_object('profile', p['id'])
            self.save_profile(profile)
            validated['profile'] += 1

        for r in self.list_objects('product'):
            self.logger.info("Validate product {}".format(r['id']))
            product = self.read_object('product', r['id'])
            self.save_product(product)
            validated['product'] += 1

        for o in self.list_objects('offer'):
            self.logger.info("Validate offer {}".format(o['id']))
            offer = self.read_object('offer', o['id'])
            self.save_offer(offer)
            validated['offer'] += 1

        self.fake_save = False
        return validated
