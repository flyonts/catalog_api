from flask import jsonify
from .. import __version__


def health_view():
    status = {
        'status': 'ok',
        'version': __version__
    }
    return jsonify(status)
