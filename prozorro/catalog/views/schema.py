from flask import abort
from . import BaseAPIView


class SchemaAPI(BaseAPIView):
    path = 'schemas'
    pk = 'schema_id'

    def get(self, schema_id=None):
        if schema_id is None:
            return self.catalog.list_schemas()
        schema = self.catalog.get_schema(schema_id)
        if schema is None:
            abort(404, 'Schema not found')
        return schema
