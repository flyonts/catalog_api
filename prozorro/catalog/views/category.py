from flask import abort, request
from . import BaseAPIView
from ..utils import get_object_or_404, require_json_data, set_access_token, validate_access_token


class CategoryAPI(BaseAPIView):
    path = 'categories'
    pk = 'category_id'

    def get(self, category_id=None):
        if category_id is None:
            return self.list_objects(self.catalog, 'category')
        category = get_object_or_404('category', category_id)
        return category['data']

    @require_json_data
    def put(self, category_id):
        data = request.json['data']
        if category_id != data['id']:
            abort(400, 'id mismatch')
        category = dict(data=data)
        set_access_token(category)
        with self.catalog as catalog:
            catalog.save_category(category, create=True)
        return category, 201, {'Location': self.get_location(category_id)}

    @require_json_data
    def patch(self, category_id):
        category = get_object_or_404('category', category_id)
        validate_access_token(category)
        data = request.json['data']
        self.catalog.validate_category_patch(data)
        category['data'].update(data)
        with self.catalog as catalog:
            catalog.save_category(category)
        return category['data']


class CategorySupplierAPI(BaseAPIView):
    path = 'categories/<category_id>/suppliers'
    pk = 'supplier_id'

    def get_supplier_or_404(self, category_id, supplier_id, ignore_missing=False, validate_token=True):
        category = get_object_or_404('category', category_id)
        if validate_token:
            validate_access_token(category)

        if not supplier_id:
            return category, None

        for s in category['data'].get('suppliers', []):
            if s['id'] == supplier_id:
                return category, s

        if ignore_missing:
            return category, None

        abort(404)

    def get(self, category_id, supplier_id=None):
        category, supplier = self.get_supplier_or_404(category_id, supplier_id, validate_token=False)
        if not supplier_id:
            return category['data'].get('suppliers', [])
        else:
            return supplier

    def delete(self, category_id, supplier_id):
        with self.catalog as catalog:
            category, supplier = self.get_supplier_or_404(category_id, supplier_id)
            supplier['status'] = 'deleted'
            catalog.save_category(category)
            return supplier

    @require_json_data
    def patch(self, category_id, supplier_id):
        with self.catalog as catalog:
            category, supplier = self.get_supplier_or_404(category_id, supplier_id)
            data = request.json['data']
            self.catalog.validate_supplier_patch(data)
            supplier.update(data)
            catalog.save_category(category)
            return supplier

    @require_json_data
    def put(self, category_id, supplier_id):
        with self.catalog as catalog:
            category, supplier = self.get_supplier_or_404(category_id, supplier_id, ignore_missing=True)
            if supplier:
                abort(400, 'Supplier %s in category %s already exists' % (supplier_id, category_id))

            data = request.json['data']

            if supplier_id != data['id']:
                abort(400, 'Supplier id mismatch')

            required_id = '{}-{}'.format(data['identifier']['scheme'], data['identifier']['id'])

            if supplier_id != required_id:
                abort(400, 'Supplier id bad format, require %s' % required_id)

            if not category['data'].get('suppliers'):
                category['data']['suppliers'] = list()

            category['data']['suppliers'].append(data)
            catalog.save_category(category)
            location = self.get_location(supplier_id, category_id=category_id)
            return data, 201, {'Location': location}
