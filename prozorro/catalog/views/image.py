import re
import os.path
from flask import abort, request
from werkzeug.utils import secure_filename
from . import BaseAPIView
from ..utils import get_object_or_404, set_access_token


class ImageAPI(BaseAPIView):
    path = 'images'
    pk = 'image_id'

    name_regex = re.compile(r'^[0-9A-Z][0-9A-Z\-_]{1,30}\.(png|jpg|jpeg)$',
                            flags=re.IGNORECASE)

    def get(self, image_id=None):
        if image_id is None:
            return self.list_objects(self.catalog, 'image')
        return get_object_or_404('image', image_id)

    def post(self):
        if 'image' not in request.files:
            abort(400, 'No image')
        image = request.files['image']
        if not image.filename:  # pragma: no cover
            abort(400, 'No filename')
        if not self.name_regex.match(image.filename):
            abort(400, 'Bad filename')
        filename = secure_filename(image.filename).lower()
        fullpath = self.catalog.image_file_path(filename)
        image_id = filename.replace('.', '_')
        image_url = self.catalog.image_file_url(filename)
        meta = {
            'id': image_id,
            'format': image.mimetype,
            'sizes': request.form.get('sizes'),
            'title': request.form.get('title'),
            'url': image_url,
        }
        obj = dict(data=meta)
        set_access_token(obj)
        with self.catalog as catalog:
            if os.path.exists(fullpath):
                abort(400, 'Image already exists')
            catalog.save_object('image', obj, create=True)
            image.save(fullpath)
        return meta, 201, {'Location': self.get_location(image_id)}
