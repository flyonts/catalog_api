from flask import abort, request
from . import BaseAPIView
from ..utils import get_object_or_404, require_json_data, set_access_token, validate_access_token


class ProfileAPI(BaseAPIView):
    path = 'profiles'
    pk = 'profile_id'

    def get(self, profile_id=None):
        if profile_id is None:
            return self.list_objects(self.catalog, 'profile')
        profile = get_object_or_404('profile', profile_id)
        return profile['data']

    @require_json_data
    def put(self, profile_id):
        data = request.json['data']
        if profile_id != data['id']:
            abort(400, 'id mismatch')
        category_id = data['relatedCategory']
        category = get_object_or_404('category', category_id)
        validate_access_token(category)
        data['id'] = profile_id
        profile = dict(data=data)
        set_access_token(profile)
        with self.catalog as catalog:
            catalog.save_profile(profile, create=True)
        return profile, 201, {'Location': self.get_location(profile_id)}

    @require_json_data
    def patch(self, profile_id):
        profile = get_object_or_404('profile', profile_id)
        validate_access_token(profile)
        data = request.json['data']
        self.catalog.validate_profile_patch(data)
        profile['data'].update(data)
        with self.catalog as catalog:
            catalog.save_profile(profile)
        return profile['data']
