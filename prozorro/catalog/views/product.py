from flask import abort, request
from . import BaseAPIView
from ..utils import get_object_or_404, require_json_data, set_access_token, validate_access_token


class ProductAPI(BaseAPIView):
    path = 'products'
    pk = 'product_id'

    def get(self, product_id=None):
        if product_id is None:
            return self.list_objects(self.catalog, 'product')
        product = get_object_or_404('product', product_id)
        return product['data']

    @require_json_data
    def put(self, product_id):
        data = request.json['data']
        if product_id != data['id']:
            abort(400, 'id mismatch')
        profile_id = data['relatedProfile']
        profile = get_object_or_404('profile', profile_id)
        validate_access_token(profile)
        data['id'] = product_id
        product = dict(data=data)
        set_access_token(product)
        with self.catalog as catalog:
            catalog.save_product(product, create=True)
        return product, 201, {'Location': self.get_location(product_id)}

    @require_json_data
    def patch(self, product_id):
        product = get_object_or_404('product', product_id)
        validate_access_token(product)
        data = request.json['data']
        self.catalog.validate_product_patch(data)
        product['data'].update(data)
        with self.catalog as catalog:
            catalog.save_product(product)
        return product['data']
