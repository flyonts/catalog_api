from flask import abort, request
from . import BaseAPIView
from ..utils import get_object_or_404, get_hsah_id, require_json_data, set_access_token, validate_access_token


class OfferAPI(BaseAPIView):
    path = 'offers'
    pk = 'offer_id'

    def get(self, offer_id=None):
        if offer_id is None:
            return self.list_objects(self.catalog, 'offer')
        offer = get_object_or_404('offer', offer_id)
        return offer['data']

    @require_json_data
    def put(self, offer_id):
        data = request.json['data']
        if 'dateModified' in data or 'id' in data:
            abort(400, 'don\'t send dateModified or id fields in offer.data')
        if offer_id != get_hsah_id(data):
            abort(400, 'hash id mismatch, require %s' % get_hsah_id(data))
        data['id'] = offer_id
        offer = dict(data=data)
        set_access_token(offer)
        with self.catalog as catalog:
            catalog.save_offer(offer, create=True)
        return offer, 201, {'Location': self.get_location(offer_id)}

    @require_json_data
    def patch(self, offer_id):
        offer = get_object_or_404('offer', offer_id)
        validate_access_token(offer)
        data = request.json['data']
        self.catalog.validate_offer_patch(data)
        # allow patch suppliers.address
        if 'suppliers' in data:
            suppliers = data.pop('suppliers')
            offer['data']['suppliers'][0].update(suppliers[0])
        offer['data'].update(data)
        with self.catalog as catalog:
            catalog.save_offer(offer)
        return offer['data']
