from base64 import urlsafe_b64encode, urlsafe_b64decode
from flask import abort, current_app, jsonify, request, url_for
from flask.views import MethodView
from werkzeug.exceptions import NotFound
from jsonschema.exceptions import ValidationError


class BaseAPIView(MethodView):
    def __init__(self, *args, **kwargs):
        super(BaseAPIView, self).__init__(*args, **kwargs)
        self.catalog = current_app.catalog

    def dump_request(self, exc):
        exc_cls = exc.__class__.__name__
        remote_addr = request.remote_addr
        if remote_addr[:4] == '127.' and request.access_route:
            remote_addr = request.access_route[-1]
        request_id = request.headers.get('X-Request-ID', '-')
        request_data = request.get_data().decode('utf-8', 'replace')
        headers = "\n".join(['%s: %s' % h for h in request.headers.items()
                            if h[0] != 'Authorization'])
        message = "%s %s %s: %s\n%s %s\n%s\n\n%s\n\n" % (
            remote_addr, request_id, exc_cls, exc,
            request.method, request.url,
            headers, request_data)
        return message

    def dispatch_request(self, *args, **kwargs):
        meth = getattr(self, request.method.lower(), None)

        # If the request method is HEAD and we don't have a handler for it
        # retry with GET.
        if meth is None and request.method == 'HEAD':
            meth = getattr(self, 'get', None)

        if meth is None:
            abort(405, self.methods)

        try:
            response = meth(*args, **kwargs)
        except Exception as exc:
            if isinstance(exc, FileNotFoundError):
                name = str(exc).rsplit('/', 1)[1]
                exc = NotFound(name.strip("'"))
            if not isinstance(exc, NotFound):
                msg = self.dump_request(exc)
                current_app.logger.exception(msg)
            if getattr(current_app, 'use_debugger', False):
                raise
            if hasattr(exc, 'code'):
                code = exc.code
            elif isinstance(exc, (AssertionError, ValueError, ValidationError)):
                code = 400
            elif isinstance(exc, PermissionError):
                code = 403
            else:
                code = 500
            err_kind = exc.__class__.__name__
            reqst_id = request.headers.get('X-Request-ID', '-')
            errormap = {'code': code, 'kind': err_kind, 'request_id': reqst_id, 'message': str(exc)[:200]}
            response = jsonify(error=errormap)
            response.status_code = code
            return response

        status_code, headers = None, None

        if isinstance(response, tuple):
            if len(response) == 2:
                response, status_code = response
            elif len(response) == 3:
                response, status_code, headers = response

        if isinstance(response, dict):
            if 'data' in response:
                response = jsonify(response)
            else:
                response = jsonify(data=response)
        elif isinstance(response, list):
            response = jsonify(data=response)

        if status_code and headers:
            return response, status_code, headers

        if status_code:
            return response, status_code

        return response

    def list_objects(self, catalog, path):
        offset = request.args.get('offset') or ''
        limit = int(request.args.get('limit') or 100)
        reverse = bool(request.args.get('reverse') or False)
        objects = catalog.list_objects(path, reverse=reverse)
        if offset and offset[:2] != '20':
            offset = urlsafe_b64decode(offset).decode()
        if offset[:2] != '20' or len(offset) < 20:
            offset = ''
        if offset and not reverse:
            objects = [obj for obj in objects if obj['dateModified'] > offset]
        elif offset and reverse:
            objects = [obj for obj in objects if obj['dateModified'] < offset]
        if limit < 1 or limit > 1000:
            limit = 100
        data = dict(data=objects[:limit])
        if data['data']:
            offset = data['data'][-1]['dateModified']
            offset = urlsafe_b64encode(offset.encode()).decode()
            data['next_page'] = dict(offset=offset)
        return data

    def get_location(self, object_id=None, *args, **kwargs):
        url = url_for(self.__class__.__name__, *args, **kwargs)
        if object_id:
            url += '/' + object_id
        return url
