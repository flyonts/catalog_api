from gevent import Timeout
from gevent.queue import Queue
from functools import partial
from rapidjson import loads, dumps
from geventwebsocket import WebSocketError


class ChangesFeedApplication(object):
    def __init__(self, ws, app):
        self.ws = ws
        self.app = app
        self.queue = Queue()
        self.max_queue = 1000
        self.recv_timeout = 1
        self.send_timeout = 10

    def push(self, item, *args, **kw):
        self.queue.put(item, *args, **kw)

    def on_changes(self):
        while len(self.queue):
            if len(self.queue) > self.max_queue:
                raise WebSocketError("Max Queue")
            data = self.queue.get()
            with Timeout(self.send_timeout, WebSocketError):
                self.ws.send(data)

    def on_message(self, message):
        data = loads(message)
        if 'ping' in data:
            resp = dict(pong=data['ping'])
            self.ws.send(dumps(resp))

    def handle(self):
        if not hasattr(self.app, 'ws_clients'):
            self.app.ws_clients = {}
        client_address = self.ws.handler.client_address
        self.app.ws_clients[client_address] = self
        try:
            while True:
                message = None
                with Timeout(self.recv_timeout, False):
                    message = self.ws.receive()
                if message:
                    self.on_message(message)
                if len(self.queue):
                    self.on_changes()
        except WebSocketError as err:
            self.app.logger.info("WebSocket %s: %s", client_address, err)
        finally:
            del self.app.ws_clients[client_address]


class ChangesFeedMiddleware(object):
    def __init__(self, app, url, flask_app):
        self.app = app
        self.url = url
        self.flask_app = flask_app

    def __call__(self, environ, start_response):
        if 'wsgi.websocket' in environ:
            ws = environ['wsgi.websocket']
            if self.url in environ['PATH_INFO']:
                ws_app = ChangesFeedApplication(ws, self.flask_app)
                ws_app.handle()
            return ["This is websocket endpoint"]
        return self.app(environ, start_response)


def changes_callback(model, data, app):
    if not app.ws_clients:
        return
    message = dumps(dict(model=model, data=data), ensure_ascii=False)
    app.logger.info("Push {} {} size {} to {} clients".format(
        model, data.get('id'), len(message), len(app.ws_clients)))
    for client in app.ws_clients.values():
        client.push(message)


def init_app(app, url):
    app.wsgi_app = ChangesFeedMiddleware(app.wsgi_app, url, app)
    callback = partial(changes_callback, app=app)
    app.catalog.set_changes_callback(callback)
    app.ws_clients = dict()
