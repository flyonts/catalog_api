FROM alpine:latest
ARG GID=1500
ARG UID=1500
RUN addgroup -g $GID catalog
RUN adduser -u $UID -G catalog -D catalog
RUN apk update
RUN apk add python3 py3-gevent libstdc++ rapidjson
RUN pip3 install --upgrade pip setuptools wheel
RUN pip3 install gunicorn[gevent]
RUN pip3 install http://dist.ed.org.ua/op/python_rapidjson-0.7.0-cp36-cp36m-linux_x86_64.whl
COPY . /app
WORKDIR app
RUN pip3 install -r requirements.txt
RUN python3 setup.py install
USER catalog
ENV CATALOG_SECRET=change-me
ENV CATALOG_CACHE=YES
ENV CATALOG_DATA=/data
EXPOSE 8000
CMD ["gunicorn", \
  "--bind", "0.0.0.0:8000", \
  "--capture-output", \
  "--error-logfile", "-", \
  "--worker-class", "geventwebsocket.gunicorn.workers.GeventWebSocketWorker", \
  "--workers", "1", \
  "--max-requests", "100000", \
  "--max-requests-jitter", "100", \
  "prozorro.catalog:create_app()"]
